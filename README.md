#Newslab Open Records Compliance Table
##Overview
- Jackie built this during her internship, it did not run until after she left. Emily updated data and made tweaks to layout.
- To update, swap out `data/data.csv` file and push files to `ajcnewsapps/2018/georgia-news-lab-table`.
- Jackie also built [accompanying viz](https://bitbucket.org/ajcnewsapp/georgia-news-lab-viz/src/master/data/data.csv) meant to run with this but it was not used
- Published 12/12/18 embedded in story [here](https://www.ajc.com/news/local-govt--politics/public-access-records-still-challenge-for-metro-atlanta-agencies/mpRTLKtCt8CQzc3bjPQtJN/)

##Author
This was created by Jacquelyn Elias in November 2018